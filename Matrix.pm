use strict;
use warnings;

use Exporter;

package Matrix;

our @ISA = qw/Exporter/;
our @EXPORT = qw/&vectorDim &matrixDim &matrixDotMatrix &matrixDotVector &addVector &subVector &normeVector &printVector &printMatrix/;

sub vectorDim($) {
  my ($vector) = @_;
  my $type = ref($vector);

  die "$type is bad array ref for $vector"
    if ($type ne "ARRAY");
  return scalar(@$vector);
}

sub matrixDim($) {
  my ($matrix) = @_;
  my $rows = vectorDim($matrix);
  my $cols = vectorDim($matrix->[0]);

  return ($rows, $cols);
}

sub matrixDotMatrix($$) {
  my ($a, $b) = @_;
  my ($rowsa, $colsa) = matrixDim($a);
  my ($rowsb, $colsb) = matrixDim($b);
  my $res = [];

  die "Matrices aren't compatible : $colsa != $rowsb"
    unless ($colsa == $rowsb);
  foreach my $i (0 .. ($rowsa - 1)) {
    foreach my $j (0 .. ($colsb - 1)) {
      foreach my $k (0 .. ($colsa - 1)) {
        $res->[$i][$j] += $a->[$i][$k] * $b->[$k][$j];
      }
    }
  }
  return $res;
}

sub matrixDotVector($$) {
  my ($matrix, $vector) = @_;
  my ($rows, $cols) = matrixDim($matrix);
  my ($max, $res) = (vectorDim($vector), []);

  die "Matrix and vector aren't compatible : $cols != $max"
    unless ($cols == $max);
  foreach my $i (0 .. ($max - 1)) {
    $res->[$i] = 0;
    foreach my $k (0 .. ($max - 1)) {
      $res->[$i] += $matrix->[$i][$k] * $vector->[$k];
    }
  }
  return $res;
}

sub addVectors($$) {
  my ($a, $b) = @_;
  my $res = [];

  die "Vectors haven't same dimension"
    unless (vectorDim($a) == vectorDim($b));
  foreach (0 .. (vectorDim($a) - 1)) {
    push(@$res, $a->[$_] + $b->[$_]);
  }
  return $res;
}

sub notVector($) {
  my ($vector) = @_;
  my $res = [];

  foreach (@$vector) {
    push(@$res, $_ * -1);
  }
  return $res;
}

sub subVectors($$) {
  return addVectors($_[0], notVector($_[1]));
}

sub normeVector($) {
  my ($vector) = @_;
  my $res = 0;

  foreach (@$vector) {
    $res += $_ ** 2;
  }
  return sqrt($res);
}

sub printMatrix($$) {
  my ($matrix, $name) = @_;

  print "$name\n";
  foreach (@$matrix) {
    foreach (@$_) {
      printf " $_ |";
    }
    print "\n";
  }
  print "--------------------------------\n";
}

sub printVector($$) {
  my ($vector, $name) = @_;

  print "$name\n";
  foreach (@$vector) {
    print " $_\n";
  }
  print "--------------------------------\n";
}

1;
