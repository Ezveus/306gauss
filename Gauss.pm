#!/usr/bin/env perl

use strict;
use warnings;

use Matrix;

package Gauss;

sub getP($$$$$) {
  my ($this, $step, $pivot, $line, $column) = @_;

  return 1 if ($line == $column);
  if ($column == $step and $line > $step) {
    my $res = -1 * $this->{matrix}->[$line][$column];
    $res /= $pivot;
    return $res;
  }
  return 0;
}

sub swapLines($$$$$) {
  my ($this, $pivot, $line, $line2, $step) = @_;

  die "Can't find a valid pivot"
    if ($$line >= $this->{dimension});
  if (${$this->{matrix}}[$$line][0] == 0) {
    if ($line2 == $this->{dimension}) {
      $$line = 0;
      $line2 = $$line + 1;
    }
    my $matrixLine = ${$this->{matrix}}[$$line];
    my $component = ${$this->{vector}}[$$line];
    ${$this->{matrix}}[$$line] = ${$this->{matrix}}[$line2];
    ${$this->{vector}}[$$line] = ${$this->{vector}}[$line2];
    ${$this->{matrix}}[$line2] = $matrixLine;
    ${$this->{vector}}[$line2] = $component;
  }
  $$pivot = ${$this->{matrix}}[$step][$step];
  $$line++;
}

sub matrixMultiplication($$$) {
  my ($this, $step, $pivot) = @_;
  my ($max, $matrix, $res) = ($this->{dimension} - 1,
                              $this->{matrix}, []);

  foreach my $i (0 .. $max) {
    foreach my $j (0 .. $max) {
      foreach my $k (0 .. $max) {
        $res->[$i][$j] += $this->getP($step, $pivot, $i, $k) *
          $matrix->[$k][$j];
      }
    }
  }
  $this->{matrix} = $res;
}

sub vectorMultiplication($$$) {
  my ($this, $step, $pivot) = @_;
  my ($max, $vector, $res) = ($this->{dimension} - 1,
                              $this->{vector}, []);

  if ($this->{debug}) {
    Matrix::printVector($this->{vector}, "vector");
    $this->printP($step, $pivot, "P vectorMultiplication");
  }
  foreach my $i (0 .. $max) {
    $res->[$i] = 0;
    foreach my $k (0 .. $max) {
      $res->[$i] += $this->getP($step, $pivot, $i, $k) *
        $vector->[$k];
      if ($this->{debug}) {
        print $this->getP($step, $pivot, $i, $k), " * $vector->[$k] + ";
      }
    }
    print "\n" if ($this->{debug});
  }
  $this->{vector} = $res;
}

sub findSolution($) {
  my ($this) = @_;
  my $step = 0;
  my $dim = $this->{dimension} - 1;

  while ($step < $dim) {
    my $line = $step;
    my $pivot = ${$this->{matrix}}[$step][$step];

    $this->swapLines(\$pivot, \$line, $line + 1, $step)
      if ($pivot == 0);
    $this->swapLines(\$pivot, \$line, $line + 1, $step)
      while ($pivot == 0);
    if ($this->{debug}) {
      print "Pivot : $pivot\n";
      print "Step $step : before multiplication per P\n";
      $this->printP($step, $pivot, "P before mult");
      $this->debug();
    }
    $this->vectorMultiplication($step, $pivot);
    $this->matrixMultiplication($step, $pivot);
    if ($this->{debug}) {
      print "Step $step : after multiplication per P\n";
      $this->debug();
    }
    $this->{solution}->[$dim] = $this->{vector}->[$dim] /
      $this->{matrix}->[$dim][$dim];
    foreach my $i (reverse(0 .. ($dim - 1))) {
      my $tmp = $this->{vector}->[$i];

      for (my $j = $dim; $j > $i; $j--) {
        $tmp -= $this->{matrix}->[$i][$j] * $this->{solution}->[$j];
      }
      $tmp /= $this->{matrix}->[$i][$i];
      $this->{solution}->[$i] = $tmp;
    }
    $step++;
  }
}

sub printP($$$$) {
  my ($this, $step, $pivot, $name) = @_;

  print "$name\n";
  foreach my $i (0 .. ($this->{dimension} - 1)) {
    foreach my $j (0 .. ($this->{dimension} - 1)) {
      print $this->getP($step, $pivot, $i, $j), " | ";
    }
    print "\n";
  }
  print "--------------------------------\n";
}

sub printSolution($) {
  my ($this) = @_;

  Matrix::printVector($this->{solution}, "x =");
}

sub norme($) {
  my ($this) = @_;
  my $res = 0;
  my $tmp = Matrix::matrixDotVector($this->{origmatrix},
                                    $this->{solution});

  $tmp = Matrix::subVectors($tmp, $this->{origvector});
  return Matrix::normeVector($tmp);
}

sub copyMatrix($) {
  my ($this) = @_;
  my $matrix = [];

  foreach (@{$this->{origmatrix}}) {
    my $line = [];
    foreach (@$_) {
      push(@$line, $_);
    }
    push(@$matrix, $line);
  }
  return $matrix;
}

sub copyVector($) {
  my ($this) = @_;
  my $vector = [];

  foreach (@{$this->{origvector}}) {
    push(@$vector, $_);
  }
  return $vector;
}

sub debug {
  my ($this, $all) = @_;

  $this->{debug} = 1;
  $this->{debug} = 2 if (defined($all) && $all eq "all");
  print "Dimension : $this->{dimension}\n";
  Matrix::printMatrix($this->{origmatrix}, "origmatrix")
      if ($this->{debug} == 2);
  Matrix::printMatrix($this->{matrix}, "matrix");
  Matrix::printVector($this->{origvector}, "origvector")
      if ($this->{debug} == 2);
  Matrix::printVector($this->{vector}, "vector");
  Matrix::printVector($this->{solution}, "solution")
      if ($this->{debug} == 2);
}

sub new($$$$) {
  my ($class, $dim, $matrix, $vector) = @_;
  my $this = {};

  bless($this, $class);
  $this->{debug} = 0;
  $this->{dimension} = $dim;
  $this->{origmatrix} = $matrix;
  $this->{matrix} = $this->copyMatrix();
  $this->{origvector} = $vector;
  $this->{vector} = $this->copyVector();
  $this->{solution} = [];
  return $this;
}
1;
