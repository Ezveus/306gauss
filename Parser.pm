use strict;

package Parser;

sub readLine($$) {
  my ($file, $linum) = @_;
  my $line = <$file>;

  $$linum++;
  chomp($line);
  return $line;
}

sub readMatrix($$$) {
  my ($this, $file, $linum) = @_;

  for (my $i = 0; $i < $this->{dimension}; $i++) {
    my $line = readLine($file, $linum);
    my @cells = split(' ', $line);
    die "Expecting $this->{dimension} cells at line $$linum"
      if (scalar(@cells) < $this->{dimension});
    push($this->{matrix}, \@cells);
  }
}

sub readVector($$$) {
  my ($this, $file, $linum) = @_;

  for (my $i = 0; $i < $this->{dimension}; $i++) {
    my $line = readLine($file, $linum);
    push($this->{vector}, int($line));
  }
}

sub parseFile($$) {
  my ($this, $filename) = @_;
  my $line = 0;
  my @keywords = qw/dimension matrice vecteur/;

  open(FILE, "<", $filename)
    or die "Can not open $filename : $!";
  die "'$keywords[0]' expected at line $line"
    if (readLine(\*FILE, \$line) ne "$keywords[0]");
  $this->{dimension} = readLine(\*FILE, \$line);
  die "A positive and non-null integer was expected at line $line" if ($this->{dimension} <= 0);
  die "'$keywords[1]' expected at line $line"
    if (readLine(\*FILE, \$line) ne "$keywords[1]");
  readMatrix($this, \*FILE, \$line);
  die "'$keywords[2]' expected at line $line"
    if (readLine(\*FILE, \$line) ne "$keywords[2]");
  readVector($this, \*FILE, \$line);
  close(FILE);
}

sub new($$) {
  my ($class, $filename) = @_;
  my $this = {};

  $this->{dimension} = 0;
  $this->{matrix} = [];
  $this->{vector} = [];

  bless($this, $class);
  parseFile($this, $filename);
  return $this;
}
1;
